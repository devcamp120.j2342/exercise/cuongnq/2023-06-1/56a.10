package com.devcamp.demo.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//Khai báo là 1 controller
@RestController
//Khai báo tiền tố của api
@RequestMapping ("/api")
//Khai báo phạm vi server có thể truy cập
@CrossOrigin
public class DemoController {
    //Khai báo api dạng Get data
    @GetMapping("/rainbow")
    //Function diễn giải cho API /rainbow
    public ArrayList<String> getRainbow() {
        ArrayList<String> rainbow = new ArrayList<>();

        rainbow.add("red");
        rainbow.add("orange");
        rainbow.add("yellow");
        rainbow.add("green");
        rainbow.add("blue");
        rainbow.add("indigo");
        rainbow.add("violet");

        return rainbow;
    }

    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam String inputString) {
        ArrayList<String> result = new ArrayList<>();
        
        String[] array_split = inputString.split(" ");

        for (String string : array_split) {
            result.add(string);
        }

        return result;
    }
}
